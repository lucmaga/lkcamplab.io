---
title: "About"
date: 2018-03-24T19:55:15-03:00
draft: false
---

Linux Kernel Campinas group study.

### Subscribe to mailing lists:
- kernelnewbies@kernelnewbies.org  
For 99% of the communication by email
- lkcamp@lists.libreplanetbr.org  
For uses that really doesn’t make sense in kernelnewbies

### Chat channel on IRC:
- #kernelnewbies @ oftc  
  For 99% of the IRC communication
- #lkcamp @ oftc or @ freenode  
For uses that really doesn’t make sense in kernelnewbies



